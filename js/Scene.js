const scene = {  
  "Scene": function () {
    this.objects = [];
    this.addObject = function (object) {
      this.objects.push(object);
    };
    this.fullScreen = function () {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
    };
    this.start = function () {
      this.canvas = graphics.createCanvas();
      this.ctx = this.canvas.getContext("2d");
      this.fullScreen();
      addEventListener("resize", () => this.fullScreen());
      document.body.appendChild(this.canvas);
    };
    this.update = function () {
      window.requestAnimationFrame(() => this.update());
      this.canvas.background("black");
      for (let i = 0; i < this.objects.length; i++) {
        this.objects[i].draw(this.ctx);
        this.objects[i].update();
        this.objects[i].move();
        if (this.objects[i].collider && this.objects[i].collider.isCollidingAny(this.objects)) {
          this.objects[i].collider.bounce();
        }
      }
    };
    this.run = function () {
      Input.start();
      this.start();
      this.update();
    };
  }
}