const Input = {
  "keysDown": {},
  "start": function () {
    addEventListener('touchstart', () => {
      const errorMessage = "No touch screen support :(";
      console.error(errorMessage);
      alert(errorMessage);
    });

    addEventListener("keydown", (e) => {
      Input.keysDown[e.keyCode] = true;
    });
  
    addEventListener("keyup", (e) => {
      delete Input.keysDown[e.keyCode];
    });
  },

  /**
   * Return -1 or 1 based on user input
   * @param {string} axis 
   */
  "GetAxis": function (axis) {
    switch (axis) {
      case "Horizontal":
        if (37 in this.keysDown || 65 in this.keysDown) {
          return -1;
        } else if (39 in this.keysDown || 68 in this.keysDown) {
          return 1;
        }
        return 0;
      case "Vertical":
        if (38 in this.keysDown || 87 in this.keysDown) {
          return -1;
        } else if (40 in this.keysDown || 83 in this.keysDown) {
          return 1;
        }
        return 0;
      default:
        return 0;
    }
  }
}