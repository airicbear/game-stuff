const graphics = {
  "createCanvas": function (canvasWidth = 500, canvasHeight = 500) {
    let newCanvas = document.createElement("canvas");
    newCanvas.ctx = newCanvas.getContext("2d");
    newCanvas.width = canvasWidth;
    newCanvas.height = canvasHeight;

    /**
     * Draw a solid color background for the canvas.
     * @param {string} color
     */
    newCanvas.background = function (color) {
      newCanvas.ctx.fillStyle = color;
      newCanvas.ctx.fillRect(0, 0, newCanvas.width, newCanvas.height);
    };
    return newCanvas;
  },
}